﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using System.IO;

/*
 * https://github.com/ChrisMaire/unity-native-sharing
 */

public class NativeShare : MonoBehaviour
{

    public string LinkURL;
    public string ShareText;
    public Texture2D Image;
    public string FileNameToSaveTo;

    //public string ScreenshotName = "screenshot.png";

    //public void ShareScreenshotWithText(string text)
    //{
    //    string screenShotPath = Application.persistentDataPath + "/" + ScreenshotName;
    //    if(File.Exists(screenShotPath)) File.Delete(screenShotPath);

    //    Application.CaptureScreenshot(ScreenshotName);

    //    StartCoroutine(delayedShare(screenShotPath, text));
    //}

    //CaptureScreenshot runs asynchronously, so you'll need to either capture the screenshot early and wait a fixed time
    //for it to save, or set a unique image name and check if the file has been created yet before sharing.
    //IEnumerator delayedShare(string screenShotPath, string text)
    //{
    //    while(!File.Exists(screenShotPath)) {
    //	    yield return new WaitForSeconds(.05f);
    //    }

    //    Share(text, screenShotPath, "");
    //}

    void SaveTextureToFile()
    {
        print("ImagE: " + Image.name);
        print("path: " + Application.persistentDataPath + "/" + FileNameToSaveTo);
        var bytes = Image.EncodeToPNG();
        var file = File.Open(Application.persistentDataPath + "/" + FileNameToSaveTo, FileMode.Create);
        var binary = new BinaryWriter(file);
        binary.Write(bytes);
        file.Close();
    }

    public void Share()
    {
        string imagePath = Application.persistentDataPath + "/" + FileNameToSaveTo;
		if (!File.Exists(imagePath))
		{
			SaveTextureToFile();
			print("saved an image to " + imagePath);
		}
		else
		{
			print("image already exists at " + imagePath);
		}


#if UNITY_ANDROID
        AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
        AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

        intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
        AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
        AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + imagePath);
        intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
        intentObject.Call<AndroidJavaObject>("setType", "image/png");

        intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), ShareText);

        AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

        AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, "");
        currentActivity.Call("startActivity", jChooser);
#elif UNITY_IOS
		CallSocialShareAdvanced(ShareText, "", LinkURL, imagePath);
#else
        Debug.Log("No sharing set up for this platform.");
#endif

	}

#if UNITY_IOS
	public struct ConfigStruct
	{
		public string title;
		public string message;
	}

	[DllImport ("__Internal")] private static extern void showAlertMessage(ref ConfigStruct conf);

	public struct SocialSharingStruct
	{
		public string text;
		public string url;
		public string image;
		public string subject;
	}

	[DllImport ("__Internal")] private static extern void showSocialSharing(ref SocialSharingStruct conf);

	public static void CallSocialShare(string title, string message)
	{
		ConfigStruct conf = new ConfigStruct();
		conf.title  = title;
		conf.message = message;
		showAlertMessage(ref conf);
	}


	public static void CallSocialShareAdvanced(string defaultTxt, string subject, string url, string img)
	{
		SocialSharingStruct conf = new SocialSharingStruct();
		conf.text = defaultTxt;
		conf.url = url;
		conf.image = img;
		conf.subject = subject;
		showSocialSharing(ref conf);
	}
#endif
}



//public class NativeShare : MonoBehaviour
//{
//	public string LinkURL;
//	public string ShareText;
//	public Texture2D Image;
//	public string FileNameToSaveTo;

//	void Start()
//	{

//	}

//	void SaveTextureToFile()
//	{
//		print("ImagE: " + Image.name);
//		print("path: " + Application.persistentDataPath + "/" + FileNameToSaveTo);
//		var bytes = Image.EncodeToPNG();
//		var file = File.Open(Application.persistentDataPath + "/" + FileNameToSaveTo, FileMode.Create);
//		var binary = new BinaryWriter(file);
//		binary.Write(bytes);
//		file.Close();
//	}

//	public void ShareBrandedImage()
//	{

//		//ShareScreenshotWithText("some text here");
//		//TODO: pull down an image from S3.

//		//     if(!File.Exists(Application.persistentDataPath + "/shareimg.png")){
//		//if (File.Exists(DefaultShareImagePath))
//		//{
//		//File.Copy(DefaultShareImagePath, Application.persistentDataPath + "/shareimg.png");
//		//print("copied the image");
//		//    } else{
//		//        print("couldn't find the image to copy across...");
//		//    }    
//		//} else{
//		//    print("the image already existss");
//		//}
//		//Share(ShareText, Application.persistentDataPath + "/shareimg.png", "");
//	}


//	public void ShareScreenshotWithText(string text)
//	{
//		//string screenShotPath = Application.persistentDataPath + "/" + ScreenshotName;
//		//if(File.Exists(screenShotPath)) File.Delete(screenShotPath);
//		//Application.CaptureScreenshot(ScreenshotName);
//		//StartCoroutine(delayedShare(ImageUrl, text));
//	}

//	//CaptureScreenshot runs asynchronously, so you'll need to either capture the screenshot early and wait a fixed time
//	//for it to save, or set a unique image name and check if the file has been created yet before sharing.
//	//IEnumerator delayedShare(string imageURL, string text)
//	//{
//	//    while(!File.Exists(screenShotPath)) {
//	//      yield return new WaitForSeconds(.05f);
//	//    }
//	//    Share(text, screenShotPath, "");
//	//}

//	public void Share()
//	{
//		string imagePath = Application.persistentDataPath + "/" + FileNameToSaveTo;
//		if (!File.Exists(imagePath))
//		{
//			SaveTextureToFile();
//			print("saved an image to " + imagePath);
//		}
//		else
//		{
//			print("image already exists at " + imagePath);
//		}


//#if UNITY_ANDROID
//        AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
//        AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");

//        intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
//        AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
//        AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + imagePath);
//        intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
//        intentObject.Call<AndroidJavaObject>("setType", "image/png");

//        intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), ShareText);

//        AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
//        AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

//        AndroidJavaObject jChooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, "");
//        currentActivity.Call("startActivity", jChooser);
//#elif UNITY_IOS
//		CallSocialShareAdvanced(ShareText, "", LinkURL, imagePath);
//#else
//        Debug.Log("No sharing set up for this platform.");
//#endif

//	}

//#if UNITY_IOS
//	public struct ConfigStruct
//	{
//		public string title;
//		public string message;
//	}

//	[DllImport("__Internal")] private static extern void showAlertMessage(ref ConfigStruct conf);

//	public struct SocialSharingStruct
//	{
//		public string text;
//		public string url;
//		public string image;
//		public string subject;
//	}

//	[DllImport("__Internal")] private static extern void showSocialSharing(ref SocialSharingStruct conf);

//	public static void CallSocialShare(string title, string message)
//	{
//		ConfigStruct conf = new ConfigStruct();
//		conf.title = title;
//		conf.message = message;
//		showAlertMessage(ref conf);
//	}


//	public static void CallSocialShareAdvanced(string defaultTxt, string subject, string url, string img)
//	{
//		SocialSharingStruct conf = new SocialSharingStruct();
//		conf.text = defaultTxt;
//		conf.url = url;
//		conf.image = img;
//		conf.subject = subject;

//		showSocialSharing(ref conf);
//	}
//#endif
//}



