﻿using UnityEngine;
using System.Collections;

public class Tutorial_number_change : MonoBehaviour {

    public GameObject Instructions;

    void OnEnable()
    {
        StartCoroutine("playSequence");
    }

    IEnumerator playSequence(){
        Instructions.SetActive(true);
        yield return new WaitForSeconds(1f);
		Gameplay.Instance.SetTarget(8);
		Gameplay.Instance.PulseTarget();
        yield return new WaitForSeconds(2f);
		Gameplay.Instance.DropBlock(5);
		Gameplay.Instance.PlayNextOnboardingStep();
    }

}
