﻿using UnityEngine;
using System;

public class Utilities : MonoBehaviour
{

    public static Vector2 roundPos(Vector2 vector)
    {
        return new Vector2(Mathf.Round(vector.x), Mathf.Round(vector.y));
    }

    public static Vector2 roundDownPos(Vector2 vector)
    {
        return new Vector2(Mathf.Floor(vector.x), Mathf.Floor(vector.y));
    }

    public static bool IsOnline()
    {
        return Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork ||
               Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork;
    }

}

[Serializable]
public class float2{
    public float x;
    public float y;

    public float2(Vector2 vector){
        x = vector.x;
        y = vector.y;
    }
}