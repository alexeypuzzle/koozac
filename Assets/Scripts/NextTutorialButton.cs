﻿using UnityEngine;

public class NextTutorialButton : MonoBehaviour {

    public void NextStep(){
        Gameplay.Instance.PlayNextOnboardingStep();
    }
}
