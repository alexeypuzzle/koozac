﻿using UnityEngine;

public class SwipeDetector : MonoBehaviour
{
    public static event SwipeHandler OnSwipeDown;
    [SerializeField] float _swipeThreshold = 50f;
    [SerializeField] float _secGapBetweenEvents = 0.2f;
    float _timeAtLastEvent;

    void Start()
    {
        _timeAtLastEvent = 0f;
    }

    void Update()
    {
		if (Input.touchCount > 0)
		{
			Touch currentTouch = Input.GetTouch(0);

			if (Mathf.Abs(currentTouch.deltaPosition.y) > _swipeThreshold)
			{
				Gameplay.Instance.ShowHighlight(false);
                if(Time.time > (_timeAtLastEvent + _secGapBetweenEvents)){
					if (OnSwipeDown != null)
					{
                        _timeAtLastEvent = Time.time;
						OnSwipeDown();
					}    
                }
			}
		}
    }
}

public delegate void SwipeHandler();