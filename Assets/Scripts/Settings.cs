﻿using UnityEngine;

public class Settings : MonoBehaviour {
    public static Settings instance;
	public int minBlockNumber;
	public int maxBlockNumber;
	public int minTarget;
	public int maxTarget;
    public float startingFallSpeed;
    public float maxFallSpeed;
    public int speedIncreaseInterval;
    public bool isDebugModeOn;
    public bool PlayTutorial;

	public bool HasRemovedAds
	{
		get
		{
			return (PlayerPrefs.GetInt(PrefsKeys.removedAds, 0) == 1);
		}
	}

	void Awake(){
        instance = this;
		minBlockNumber = PlayerPrefs.GetInt(PrefsKeys.minBlock, 1);
		maxBlockNumber = PlayerPrefs.GetInt(PrefsKeys.maxBlock, 4);
        minTarget = PlayerPrefs.GetInt(PrefsKeys.minTarget, 3);
        maxTarget = PlayerPrefs.GetInt(PrefsKeys.maxTarget, 6);
        startingFallSpeed = PlayerPrefs.GetInt(PrefsKeys.startingS, 1);
        maxFallSpeed = PlayerPrefs.GetInt(PrefsKeys.maxS, 9);
        speedIncreaseInterval = PlayerPrefs.GetInt(PrefsKeys.maxS, 3);
        isDebugModeOn = (PlayerPrefs.GetInt(PrefsKeys.DebugMode, 0) == 1);
    }

	void Start()
	{
		#if UNITY_EDITOR
		PlayerPrefs.DeleteAll();
		
		#endif
	}
}
