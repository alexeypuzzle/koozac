﻿﻿using System.Collections.Generic;
using UnityEngine;

public class grid : MonoBehaviour {

    public static int width = 7;
    public static int height = 7;
    public static Block[,] data = new Block[width, height];

    
    public static bool isInsideBorders(Vector2 pos){
		return ((int)pos.x >= 0 &&
			(int)pos.x < width &&
			(int)pos.y >= 0);
    }

    public static bool canMoveDown(Vector2 pos){
		int clampedColumn = (int)Mathf.Clamp(pos.x, 0f, (width - 1));
		float minimumY = GetHighestRowWithABlockInColumn(clampedColumn);
	    // let the block move up to the height of the grid.
	    if (minimumY == -1)
	    {
		    return pos.y > height;
	    }
        if (pos.y > minimumY){
           return true;
        }
        return false;
	}

    public static bool CanMoveToColumn(Vector2 pos){
        int clampedColumn = (int)Mathf.Clamp(pos.x, 0f, width-1);
        float minimumY = GetHighestRowWithABlockInColumn(clampedColumn);
		print("pos y: " + pos.y);
	    print("min: " + minimumY);
		if (pos.y >= minimumY)
		{
			return true;
		}
	    print("can move to column false");

		return false;
    }
	
	/// <summary>
	/// Returns -1 if a column is full.
	/// </summary>
	/// <param name="col"></param>
	/// <returns></returns>
    public static float GetHighestRowWithABlockInColumn(int col){
        float HighestRow = 0f;
		for (int y = 0; y <= height; y++)
		{
			if (y == height) return -1;
			if (data[col, y] != null) continue;
			HighestRow = y;
			break;
		}
        return HighestRow;
    }

    public static bool TrySpawn(Vector3 pos){
        var clampedColumn = (int)Mathf.Clamp(pos.x, 0f, width - 1);
		var minimumY = GetHighestRowWithABlockInColumn(clampedColumn);
        return pos.y >= minimumY;
    }

    /// <summary>
    ///  returns true if block placed was a "correct" answer, false if the answer couldn't be made. Also false if placed on a full column.
    /// </summary>
    public static bool PlaceAndCheckForBlocksToDestroy(Block block){
        Vector2 blockPos = block.transform.position;
        int clampedColumn = (int)Mathf.Clamp(blockPos.x, 0f, width - 1);
        float minimumY = GetHighestRowWithABlockInColumn(clampedColumn);
	    if (minimumY != -1)
	    {
		    block.transform.position = new Vector2(blockPos.x, minimumY);
	    }
	    else
	    {
		    print("game over!");
		    // block placed on top of a full row. return false.
		    block.transform.position = new Vector2(blockPos.x, height);
		    return false;
	    }
		Vector2 newBlockPos = block.transform.position;
		data[(int)newBlockPos.x, (int)newBlockPos.y] = block;
        int total = 0;
        int target = Gameplay.Instance.CurrentTarget;
        int numberOfBlocksToDestroy = -1;
        for (int y = (int)newBlockPos.y; y >= 0; y--)
        {
            numberOfBlocksToDestroy++;
            total += data[(int)newBlockPos.x, y].value;
            if(total == target && numberOfBlocksToDestroy > 0){
                for (int j = 0; j <= numberOfBlocksToDestroy; j++)
                {
                    data[(int)newBlockPos.x, (int)newBlockPos.y - j].Explode();
					data[(int)newBlockPos.x, (int)newBlockPos.y - j] = null;
                }
                return true;
            }
            if(total > target){
                return false;
            }
        }
        return false;
    }

	public static void DestroyRowsFromTop(int numberOfRows)
	{
		for (int i = 0; i < width -1; i++)
		{
			for (int j = height - 1; j > height - 1 - numberOfRows; j--)
			{
				print("checking " + i + " / " + j);
				if (data[i, j] != null)
				{
					data[i, j].Explode();
					data[i, j] = null;
				}
			}
		}
	}

    public static void clearGrid(){
        for (int i = 0; i <= width -1 ; i++)
        {
            for (int j = 0; j <= height -1; j++)
            {
                data[i,j] = null;
            }
        }
    }
}
