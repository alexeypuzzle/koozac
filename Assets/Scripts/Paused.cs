﻿using UnityEngine;
using UnityEngine.UI;

public class Paused : MonoBehaviour {

    public Toggle VibrationToggle;
    public Toggle SoundToggle;

    void Start()
    {
        VibrationToggle.isOn = (PlayerPrefs.GetInt(PrefsKeys.vibration, 0) == 1);
        SoundToggle.isOn = (PlayerPrefs.GetInt(PrefsKeys.sound, 0) == 1);
    }

    public void OnClosePressed(){
        Viker_uicontroller.Instance.ClosePopup();
        Gameplay.Instance.ResumeGame();
    }

    public void OnVibrationToggled(bool isOn){
        PlayerPrefs.SetInt(PrefsKeys.vibration, (isOn)? 1: 0);
    }

    public void OnSoundEffectsToggled(bool isOn){
        PlayerPrefs.SetInt(PrefsKeys.sound, (isOn) ? 1 : 0);
    }

    public void OnEndGamePressed(){
        OnClosePressed();
        Gameplay.Instance.OnEndGamePressed();
    }

    public void OnPurchaseNoAdsPressed(){
        PurchaseController.instance.BuyNoAds();
    }
}
