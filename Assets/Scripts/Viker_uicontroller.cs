﻿using UnityEngine;
using System.Collections.Generic;

public class Viker_uicontroller : MonoBehaviour {

	public static Viker_uicontroller Instance;
	public GameObject ActiveUi;
	public GameObject ActiveOverlay;
	public Transform PopupLayer;
	public bool IsInGame;
	public bool DebugMode;
	public GameObject HomepageSettings;
	public GameObject GameplayPause;
	public GameObject DebugMenu;
	public GameObject AmazeballsPromo;
	const string CloneString = "(Clone)";
	Stack<string> _history;
	bool _showingPopup = false;
	GameObject _activePopup;
	
	void Awake()
	{
		Application.targetFrameRate = 30;
		Instance = this;
		_history = new Stack<string>();
	}

	void Start()
	{
		GameObject debugScene = GameObject.FindGameObjectWithTag("UI");
		if (debugScene != null)
		{
			print("debugging a scene...");
			ActiveUi = debugScene;
		}
		else
		{
			print("no UI scene found");
            LoadUi(K_UI.MainMenu);
		}
	}

	public void ToggleDebugMode()
	{
        if (PlayerPrefs.GetInt(PrefsKeys.DebugMode) == 1)
		{
			PlayerPrefs.SetInt(PrefsKeys.DebugMode, 0);
			DebugMode = false;
		}
		else
		{
			PlayerPrefs.SetInt(PrefsKeys.DebugMode, 1);
			DebugMode = true;
		}
	}

	public void ShowPopup(string popupName){
        if(popupName == K_UI.HomepageSettings){
            _activePopup = HomepageSettings;
        } else if(popupName == K_UI.Debug){
            _activePopup = DebugMenu;
        } else if (popupName == K_UI.Paused){
            _activePopup = GameplayPause;
        } else if (popupName == K_UI.AmazeballsPromo)
        {
	        _activePopup = AmazeballsPromo;
        }
        _activePopup.SetActive(true);
    }

    public void ClosePopup(){
        if(_activePopup != null){
            _activePopup.SetActive(false);
            _activePopup = null;
        } else{
            Debug.LogError("tried to close a popup when one wasn't active.");
        }
    }

	public void LoadUi(string newUi, bool clearHistory = false)
	{
		if (ActiveOverlay != null)
		{
			Destroy(ActiveOverlay);
		}

		if (ActiveUi != null && !clearHistory)
		{
			_history.Push(ActiveUi.name);
			Destroy(ActiveUi);
		}
		else if (clearHistory)
		{
			Destroy(ActiveUi);
		}
		ActiveUi = Instantiate((GameObject)Resources.Load(newUi));
		print("made a " + newUi);
		if (ActiveUi.name.EndsWith(CloneString))
		{
			
			ActiveUi.name = ActiveUi.name.Substring(0, ActiveUi.name.Length - CloneString.Length);
		}
		if (clearHistory)
		{
			_history.Clear();
		}
	}

	public void GoBack()
	{
        if(_showingPopup){
            ClosePopup();
            return;
        }

		if (_history.Count == 0)
		{
			Application.Quit();
			return;
		}
		
		string previousScreen = _history.Pop();

		if (ActiveUi != null)
		{
			Destroy(ActiveUi);
		}
		ActiveUi = Instantiate((GameObject)Resources.Load(previousScreen));
		if (ActiveUi.name.EndsWith(CloneString))
		{
			ActiveUi.name = ActiveUi.name.Substring(0, ActiveUi.name.Length - CloneString.Length);
		}
	}
//
//	void PrintHistory()
//	{
//		string historyString = "history : ";
//		foreach (var scene in _history)
//		{
//			historyString = historyString + scene + ", ";
//		}
//		print(historyString);
//	}

#if UNITY_ANDROID || UNITY_EDITOR
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			GoBack();
		}
	}
#endif
}
