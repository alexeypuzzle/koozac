﻿using UnityEngine;

public class PointsText : MonoBehaviour {

    void OnEnable()
    {
        iTween.MoveBy(gameObject, iTween.Hash("amount", new Vector3(0f, 1f, 0f), "time", 1f, "oncomplete", "onDone"));
    }

    void onDone(){
        gameObject.SetActive(false);
    }
}
