﻿using UnityEngine;

public class Pulse : MonoBehaviour {

    public void Play(){
        iTween.ScaleTo(gameObject, iTween.Hash("scale", new Vector3(2f, 2f, 2f), "time", 1f));
        iTween.ScaleTo(gameObject, iTween.Hash("delay", 1f, "scale", new Vector3(1f, 1f, 1f), "time", 1f));
    }
}
