﻿using UnityEngine;
using System.Collections;

public class Tutorial_swipe_to_move : MonoBehaviour {

    public GameObject Hand;
    public GameObject Instructions;
	[SerializeField] float _maxsSpeed = 1f;
	float _dampener;
    bool _waitingForSwipe;

    void OnEnable()
    {
        StartCoroutine("playSequence");
    }

    IEnumerator playSequence(){
	    Gameplay.Instance.EnableSwipeDown(false);
	    Gameplay.Instance.EnableColumnChange(false);
        yield return new WaitForSeconds(2f);
	    Gameplay.Instance.EnableColumnChange(true);
	    Gameplay.Instance.EnableSwipeDown(true);
		Instructions.SetActive(true);
		iTween.MoveBy(Hand, iTween.Hash("amount", new Vector3(Screen.width / 3, 0, 0f), "time", 1f, "looptype", iTween.LoopType.loop));
        UpdateDampener(0f);
        _waitingForSwipe = true;
        while (_waitingForSwipe)
        {
            yield return new WaitForSeconds(0.2f);
        }
        Instructions.SetActive(false);
        UpdateDampener(1f);
    }

    void UpdateDampener(float newNumber)
	{
		_dampener = newNumber;
		Gameplay.Instance.FallSpeed = _maxsSpeed * _dampener;
	}

    void Update()
    {
        if(_waitingForSwipe){
			if (Input.GetMouseButton(0))
			{
                _waitingForSwipe = false;
			}    
        }
    }
}
