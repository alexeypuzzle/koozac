﻿using UnityEngine;
using admob;
using UnityEngine.Advertisements;
using System.Collections;
using System;

public class Viker_adsController : MonoBehaviour
{
    [SerializeField]  string _unityAndroidGameId;
	[SerializeField]  string _unityiosGameId;
	public static Viker_adsController Instance;
    public string iOSBannerId;
    public string iOSStaticInterstitialId;
    public string AndroidBannerId;
    public string AndroidStaticInterstitialId;
    public static event EventHandler OnAdWatched;

    string _bannerId;
    string _interstitialId;

    void Awake()
    {
        Instance = this;
#if UNITY_IOS || UNITY_EDITOR
	    _bannerId = iOSBannerId;
	    _interstitialId = iOSStaticInterstitialId;
#elif UNITY_ANDROID || UNITY_EDITOR
        _bannerId = AndroidBannerId;
        _interstitialId = AndroidStaticInterstitialId;
#endif
	}

    void Start(){
		Admob ad = Admob.Instance();
		ad.initAdmob(_bannerId, _interstitialId);
        ad.loadInterstitial();
		//ad.bannerEventHandler += onBannerEvent;
		ad.interstitialEventHandler += onInterstitialEvent;
		//ad.rewardedVideoEventHandler += onRewardedVideoEvent;
		//ad.nativeBannerEventHandler += onNativeBannerEvent;
        StartCoroutine("InitAds");
    }

	IEnumerator InitAds()
	{

		while ((!Advertisement.isInitialized || !Advertisement.IsReady("rewardedVideo")) && isOnline())
		{
#if UNITY_ANDROID
            Advertisement.Initialize(_unityAndroidGameId, false);
#elif UNITY_IOS
            Advertisement.Initialize(_unityiosGameId, false);
#endif
			yield return new WaitForSeconds(0.5f);
		}
	}

    public void ShowRewardedAd(Action<ShowResult> newCallback)
	{
		if (Advertisement.IsReady("rewardedVideo") && Utilities.IsOnline())
		{
			var options = new ShowOptions { resultCallback = newCallback };
			Advertisement.Show("rewardedVideo", options);
		}
	}

    public void ShowInterstitial(){
        print("adscontroller show interstitial.");
        if(Admob.Instance().isInterstitialReady()){
            Admob.Instance().showInterstitial();
            print("it's ready.");
        } else{
            print("it's NOT ready.");
            if(OnAdWatched != null){
                OnAdWatched();
                print("called ad watched");
            }
        }
    }
//
//    public void ShowSkipToLevelVideo(){
//		const string RewardedPlacementId = "skipToLevel";
//		if (!Advertisement.IsReady(RewardedPlacementId) || !isOnline())
//		{
//			Debug.Log(string.Format("Ads not ready for placement '{0}'", RewardedPlacementId));
//			HandleShowResult(ShowResult.Finished);
//			return;
//		}
//
//		var options = new ShowOptions { resultCallback = HandleShowResult };
//		Advertisement.Show(RewardedPlacementId, options);  
//    }

	public void ShowRewardedAd()
	{
		const string RewardedPlacementId = "rewardedVideo";
        if (!Advertisement.IsReady(RewardedPlacementId) || !isOnline())
		{
			Debug.Log(string.Format("Ads not ready for placement '{0}'", RewardedPlacementId));
			HandleShowResult(ShowResult.Finished);
			return;
		}
		print("ad is ready");
		var options = new ShowOptions { resultCallback = HandleShowResult };
		Advertisement.Show(RewardedPlacementId, options);
	}


	void HandleShowResult(ShowResult result)
	{
		switch (result)
		{
			case ShowResult.Finished:
				Debug.Log("The ad was successfully shown.");
				if (OnAdWatched != null)
				{
					OnAdWatched();
				}
                Admob.Instance().loadInterstitial();
				break;
			case ShowResult.Skipped:
				Debug.Log("The ad was skipped before reaching the end.");
				if (OnAdWatched != null)
				{
					OnAdWatched();
				}
                Admob.Instance().loadInterstitial();
				break;
			case ShowResult.Failed:
				Debug.LogError("The ad failed to be shown.");
                Admob.Instance().loadInterstitial();
				break;
		}
	}

    public void HideBanner(){
        Admob.Instance().removeBanner();
        Debug.LogWarning("Hide banner");
    }

    public void ShowBanner(){
		Admob.Instance().showBannerRelative(AdSize.SmartBanner, AdPosition.BOTTOM_CENTER, 0);
        Debug.LogWarning("Show banner");
	}

	//void onBannerEvent(string eventName, string msg)
	//{
	//	Debug.Log("handler onAdmobBannerEvent---" + eventName + "   " + msg);
	//}

	void onInterstitialEvent(string eventName, string msg)
	{
		Debug.Log("handler onAdmobEvent---" + eventName + "   " + msg);
		if (eventName == AdmobEvent.onAdLoaded)
		{
			if (OnAdWatched != null)
			{
				OnAdWatched();
			}
            print("interstitial loaded");
        } else if(eventName == AdmobEvent.onAdClosed){
            print("interstitial closed");
            if(OnAdWatched!= null){
                OnAdWatched();
                Admob.Instance().loadInterstitial();
            }
        }
	}

	bool isOnline()
	{
		if (Application.internetReachability == NetworkReachability.NotReachable)
		{
			return false;
		}
		return true;
	}

	void OnApplicationPause(bool pause)
	{
		if (!pause)
		{
			StartCoroutine("InitAds");
		}
	}
}