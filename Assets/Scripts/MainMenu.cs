﻿using UnityEngine;

public class MainMenu : MonoBehaviour {

	void Start()
	{
		Viker_adsController.Instance.HideBanner();
	}
	
    public void onPlayPressed(){
	    Viker_uicontroller.Instance.LoadUi(K_UI.Gameplay);
    }

	public void onSettingsPressed()
	{
		Viker_uicontroller.Instance.ShowPopup(K_UI.HomepageSettings);
	}

	public void OnNoAdsPressed()
	{
		PurchaseController.instance.BuyNoAds();
	}

	public void OnPromoPressed()
	{
		Viker_uicontroller.Instance.ShowPopup(K_UI.AmazeballsPromo);	
	}
}
