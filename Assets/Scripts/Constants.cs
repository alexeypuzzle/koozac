﻿public class K_UI{
    // ui strings must match prefab names
    public static readonly string MainMenu = "MainMenu";
    public static readonly string Gameplay = "Gameplay";
    public static readonly string Settings = "Settings";
    public static readonly string HomepageSettings = "HomepageSettings";
	public static readonly string Paused = "Paused";
	public static readonly string Debug = "Debug";
    public static readonly string AmazeballsPromo = "Amazeballs";
}

public class PrefsKeys{
    public static readonly string DebugMode = "d";
    public static readonly string minBlock = "minB";
    public static readonly string maxBlock = "maxB";
    public static readonly string minTarget = "minT";
    public static readonly string maxTarget = "maxT";
    public static readonly string startingS = "startS";
    public static readonly string maxS = "maxS";
    public static readonly string sInterval = "sInterval";
    public static readonly string vibration = "v";
    public static readonly string sound = "s";
    public static readonly string removedAds = "a";
    public static readonly string highScore = "h";
}

public class K_URLS
{
    public static readonly string playStoreURL = "http://viker.co.uk";
    public static readonly string appStoreURL = "http://viker.co.uk";
    public static readonly string Amazeballs_ios = "https://itunes.apple.com/gb/app/amazeballs-puzzle-block-game/id1219662447";
    public static readonly string Amazeballs_android = "https://play.google.com/store/apps/details?id=com.viker.amazeballs";
}
