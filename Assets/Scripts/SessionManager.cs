﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

public class SessionManager : MonoBehaviour
{

    public static SessionManager instance;
    public Session session;
    public bool IsContinuingSession { get { return _isContinuingSession; } }
    bool _isContinuingSession;

    void Awake()
    {
        instance = this;
    }

    private void OnGUI()
    {
#if UNITY_EDITOR
		GUILayout.BeginArea(new Rect((Screen.width / 2) - 100, 0, 200, 200));
			if (GUILayout.Button("SAVE"))
			{
				SaveSession();
			}
		GUILayout.EndArea();

#endif
    }

    void Start()
	{
		if (LoadSession())
		{
			_isContinuingSession = true;
			//Viker_UIController.instance.LoadUI(Viker_UI.Gameplay);
		}
	}

	public void ClearSession()
	{
		session = null;
		_isContinuingSession = false;
		if (File.Exists(Application.persistentDataPath + "/game.state"))
		{
			File.Delete(Application.persistentDataPath + "/game.state");
		}
	}

	bool LoadSession()
	{
		if (File.Exists(Application.persistentDataPath + "/game.state"))
		{
			BinaryFormatter binary = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/game.state", FileMode.Open);
			session = (Session)binary.Deserialize(file);
			file.Close();
			Debug.LogWarning("Session found!");
			return true;
		}
		else
		{
			Debug.LogError("No session found");
			return false;
			//session = new Session();
		}
	}

	public void SaveSession()
	{
		session = new Session();
        for (int i = 0; i <= grid.width - 1; i++)
        {
            for (int j = 0; j <= grid.height - 1; j++)
            {
                if (grid.data[i, j] != null)
                {
                    session.blocks.Add(grid.data[i,j].GetSavedata());
                }
            }
        }
        print("session saved!");
        printState();
        BinaryFormatter binary = new BinaryFormatter();
		FileStream file = File.Create(Application.persistentDataPath + "/game.state");
		binary.Serialize(file, session);
		file.Close();
	}

	void printState()
	{

        for (int i = 0; i < session.blocks.Count; i++)
        {
            print(session.blocks[i].Pos + " : " + session.blocks[i].Value);
        }
    }

	public bool HasASavedSession()
	{
		return (session != null);
	}

	public void OnSessionRestored()
	{
		_isContinuingSession = false;
	}
}

[Serializable]
public class Session
{
    public List<BlockData> blocks;
    public Session(){
        blocks = new List<BlockData>();
    }
	//public float3 ballsPos;
	//public int ballCount;
	//public int starCount;
	//public List<blockData> blockData;
	//public List<PowerUpData> powerUpData;
	//public List<StarData> starData;
	//public bool isFirstAttempt;
	//public Session()
	//{
	//	powerUpData = new List<PowerUpData>();
	//	blockData = new List<blockData>();
	//	starData = new List<StarData>();
	//}
}

