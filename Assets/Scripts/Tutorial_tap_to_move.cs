﻿using System.Collections;
using UnityEngine;

public class Tutorial_tap_to_move : MonoBehaviour
{
    public GameObject Instructions;
    public Transform TapIcon;
    int columnChosen;
    bool waitingForTap;
    float dampener = 0.1f;
    float maxSpeed = 1f;

    void OnEnable()
    {
        Gameplay.Instance.DropBlock(1);
        //columnChosen = 5;
        columnChosen = Gameplay.Instance.ColumnForTutorial;
        StartCoroutine("playSequence");
    }

    IEnumerator playSequence(){
        yield return new WaitForSeconds(2f);
		Gameplay.Instance.EnableColumnChange(false);
		updateDampener(0f);
        Instructions.SetActive(true);
        positionTapToColumnChosen();
        waitingForTap = true;
        while(waitingForTap){
            yield return new WaitForSeconds(0.2f);
        }
        updateDampener(1f);
        Instructions.SetActive(false);
        TapIcon.gameObject.SetActive(false);
    }

    void positionTapToColumnChosen(){
        Vector3 tapPointInWorld = new Vector3(columnChosen + 0.5f, 3.5f, 0f);
        TapIcon.position = Camera.main.WorldToScreenPoint(tapPointInWorld);
        TapIcon.gameObject.SetActive(true);
    }

	void updateDampener(float newNumber)
	{
		dampener = newNumber;
		Gameplay.Instance.FallSpeed = maxSpeed * dampener;
	}

    void Update()
    {
        if(waitingForTap){
            if(Input.touchCount > 0)
            {
                // check that the dude has tapped the right column.
                Vector3 worldTap = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
                if( Utilities.roundDownPos(worldTap).x == columnChosen )
                {
                    Gameplay.Instance.MoveCurrentBlockToColumn(columnChosen);
                    waitingForTap = false;
                }
            }
        }
    }
}

