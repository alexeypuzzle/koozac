﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial_second_block : MonoBehaviour {

    public GameObject Instructions;
    float dampener = 0.1f;
    float maxSpeed = 1f;

    void OnEnable()
    {
		Gameplay.Instance.DropBlock(1);
		iTween.ValueTo(gameObject, iTween.Hash("from", dampener, "to", 1f, "time", 1f, "onupdate", "updateDampener", "easetype", iTween.EaseType.easeInQuint));
		StartCoroutine("playSequence");
	}

    IEnumerator playSequence(){
        Gameplay.Instance.EnableSwipeDown(false);
        Gameplay.Instance.EnableColumnChange(false);
		yield return new WaitForSeconds(2f);
		updateDampener(0f);
		Instructions.SetActive(true);
		yield return new WaitForSeconds(2f);
		updateDampener(1f);
		Instructions.SetActive(false);
		yield return new WaitForSeconds(2f);
		Gameplay.Instance.PlayNextOnboardingStep();
    }

	void updateDampener(float newNumber)
	{
		dampener = newNumber;
		Gameplay.Instance.FallSpeed = maxSpeed * dampener;
	}
}
