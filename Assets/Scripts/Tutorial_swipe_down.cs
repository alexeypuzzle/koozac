﻿using UnityEngine;

public class Tutorial_swipe_down : MonoBehaviour {

    public GameObject Hand;
    public GameObject Instructions;
    [SerializeField] float maxsSpeed = 1f;

    void OnEnable()
    {
        Gameplay.Instance.EnableSwipeDown(true);
        SwipeDetector.OnSwipeDown += onSwipe;
        Instructions.SetActive(true);
        Gameplay.Instance.FallSpeed = maxsSpeed * 0f;
        iTween.MoveBy(Hand, iTween.Hash("amount", new Vector3(0f, -100f, 0f), "time", 1f, "looptype", iTween.LoopType.loop));
    }

	void showInstructions(bool isShowing)
	{
		Instructions.SetActive(isShowing);
	}

	public void onSwipe()
	{
		Gameplay.Instance.FallSpeed = maxsSpeed * 1f;
		showInstructions(false);
        SwipeDetector.OnSwipeDown -= onSwipe;
        Gameplay.Instance.EnableSwipeDown(false);
	}
}
