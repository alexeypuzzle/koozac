﻿using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;

public class Viker_FacebookController : MonoBehaviour {

	public static Viker_FacebookController instance;

	void Awake()
	{
		instance = this;
	}

	void Start()
	{
		if (FB.IsInitialized)
		{
			FB.ActivateApp();
		}
		else
		{
            FB.Init(OnInitComplete);
		}
	}

	void OnApplicationPause(bool pauseStatus)
	{
		if (!pauseStatus)
		{
			if (FB.IsInitialized)
			{
				FB.ActivateApp();
			}
			else
			{
                FB.Init(OnInitComplete);
			}
		}
	}

    void OnInitComplete()
    {
        FB.ActivateApp();
    }
}
