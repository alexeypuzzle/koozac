﻿using UnityEngine;
using UnityEngine.UI;

public class DebugMenu : MonoBehaviour {

    public Slider MinBlock;
    public Slider MaxBlock;
	public Text minBlockText;
	public Text maxBlockText;

    public Slider MinTarget;
    public Slider MaxTarget;
    public Text minTargetText;
    public Text maxTargetText;

	public Slider StartFallSpeed;
	public Slider MaxFallSpeed;
    public Slider FallSpeedInterval;
	public Text startSText;
	public Text maxSText;
    public Text sIntervalText;

    public Toggle DebugToggle;

    void Start()
    {
        MinBlock.value = Settings.instance.minBlockNumber;
        MaxBlock.value = Settings.instance.maxBlockNumber;
        MinTarget.value = Settings.instance.minTarget;
        MaxTarget.value = Settings.instance.maxTarget;
        StartFallSpeed.value = Settings.instance.startingFallSpeed;
        MaxFallSpeed.value = Settings.instance.maxFallSpeed;
        FallSpeedInterval.value = Settings.instance.speedIncreaseInterval;
        DebugToggle.isOn = Settings.instance.isDebugModeOn;
        print("in debug menu start. is on: " + Settings.instance.isDebugModeOn);
    }


    public void OnMinBlockValueChanged(float newValue){
        Settings.instance.minBlockNumber = (int)newValue;
        minBlockText.text = Settings.instance.minBlockNumber.ToString();
        PlayerPrefs.SetInt(PrefsKeys.minBlock, (int)newValue);
    }

	public void OnMaxBlockValueChanged(float newValue)
	{
        Settings.instance.maxBlockNumber = (int)newValue;
        maxBlockText.text = Settings.instance.maxBlockNumber.ToString();
        PlayerPrefs.SetInt(PrefsKeys.maxBlock, (int)newValue);
	}
	public void OnMinTargetChanged(float newValue)
	{
        Settings.instance.minTarget = (int)newValue;
        minTargetText.text = Settings.instance.minTarget.ToString();
        PlayerPrefs.SetInt(PrefsKeys.minTarget, (int)newValue);

	}
	public void OnMaxTargetChanged(float newValue)
	{
        Settings.instance.maxTarget = (int)newValue;
        maxTargetText.text = Settings.instance.maxTarget.ToString();
		PlayerPrefs.SetInt(PrefsKeys.maxTarget, (int)newValue);
	}

	public void OnStartSpeedChanged(float newValue)
	{
        Settings.instance.startingFallSpeed = newValue;
        startSText.text = Settings.instance.startingFallSpeed.ToString();
        PlayerPrefs.SetInt(PrefsKeys.startingS, (int)newValue);
	}

	public void OnMaxSpeedChanged(float newValue)
	{
        Settings.instance.maxFallSpeed = newValue;
        maxSText.text = Settings.instance.maxFallSpeed.ToString();
        PlayerPrefs.SetInt(PrefsKeys.maxS, (int)newValue);
	}

	public void OnFallSpeedIntervalChanged(float newValue)
	{
        Settings.instance.speedIncreaseInterval = (int)newValue;
        sIntervalText.text = Settings.instance.speedIncreaseInterval.ToString();
        PlayerPrefs.SetInt(PrefsKeys.sInterval, (int)newValue);
	}

    public void OnDebugInfoPressed(bool isOn){
        Settings.instance.isDebugModeOn = isOn;
        PlayerPrefs.SetInt(PrefsKeys.DebugMode, (isOn) ? 1 : 0);
        print("changed debug menu to " + Settings.instance.isDebugModeOn);
    }

    public void OnBackPressed(){
        Gameplay.Instance.ResumeGame();
	    Viker_uicontroller.Instance.ClosePopup();
    }

}
