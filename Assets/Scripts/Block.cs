﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class Block : MonoBehaviour {

    public Text valueText;
    public int value;
    public bool isFalling;
    public float fallSpeed;
    public GameObject HighlightPrefab;
    public Image image;
    bool _isSlammingDown;
    bool _canChangeColumns = true;
    bool _canSwipeDown = true;
    float _speedMultiplier = 1f;

    void Start(){
        //TODO remove this listener when creating from memory.
        isFalling = true;
        SwipeDetector.OnSwipeDown += slamDown;
    }

    void Update()
    {
        if (!isFalling) return;
        if(!Gameplay.Instance.IsPaused){
            MoveDown();
            if(!_isSlammingDown){
                checkForColumnMove();    
            }
        }
    }

    protected void MoveDown(){
        Vector2 DownByOne = transform.position + (Vector3.down * Time.deltaTime * Gameplay.Instance.FallSpeed * _speedMultiplier);
		if (grid.canMoveDown(DownByOne))
		{
            transform.position += Vector3.down * Time.deltaTime * Gameplay.Instance.FallSpeed * _speedMultiplier;
        } else{
			isFalling = false;
			Gameplay.Instance.OnBlockPlaced();
            SwipeDetector.OnSwipeDown -= slamDown;
		}
    }
    void checkForColumnMove()
    {
        if (!_canChangeColumns) return;
        if (Input.touchCount > 0)
        {
            Vector2 roundedTouchPosInWorld = Utilities.roundDownPos(Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position));
            if (!grid.isInsideBorders(roundedTouchPosInWorld)) return;
            var minimumYForColum = grid.GetHighestRowWithABlockInColumn((int) roundedTouchPosInWorld.x);
            if (!(transform.position.y > minimumYForColum)) return;
            transform.position = new Vector2(roundedTouchPosInWorld.x, transform.position.y);
            Gameplay.Instance.ShowHighlight(true, roundedTouchPosInWorld.x);
        }
        else
        {
            Gameplay.Instance.ShowHighlight(false);
        }
    }

    public void setValueText(int newValue)
    {
        value = newValue;
        valueText.text = value.ToString();
    }

    public void slamDown()
    {
        if(_canSwipeDown){
			_isSlammingDown = true;
			_speedMultiplier = 15f;    
        }
    }

    public void EnableColumnChange(bool isOn){
        _canChangeColumns = isOn;
    }

	public void SetSwipeDown(bool isEnabled)
	{
        _canSwipeDown = isEnabled;
	}

    public void MoveToColumn(int column){
        transform.position = new Vector3(column, transform.position.y, transform.position.z);
    }

    public BlockData GetSavedata()
    {
        BlockData data = new BlockData();
        data.Pos = new float2(Utilities.roundPos(transform.position));
        data.Value = value;
        return data;
    }

    public void Explode(){
        GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
        transform.position = new Vector3(transform.position.x + 0.5f, transform.position.y + 0.5f, transform.position.z);
        iTween.ScaleTo(gameObject, iTween.Hash("scale", new Vector3(0f, 0f, 0f), "time", 1f, "oncomplete", "OnExplodeFinish"));
    }

    void OnExplodeFinish(){
        Destroy(gameObject);
    }
}

[Serializable]
public class BlockData{
    public float2 Pos;
    public int Value;
}


