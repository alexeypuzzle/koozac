﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GotostoreButton : MonoBehaviour
{
	string _storeLink;
	
	void Start () {
		
		#if UNITY_ANDROID
		_storeLink = K_URLS.playStoreURL;
		
		#elif UNITY_IOS
		_storeLink = K_URLS.appStoreURL;
		#endif
	}

	public void GoToStore()
	{
		Application.OpenURL(_storeLink);
	}
}
