﻿using UnityEngine;
using UnityEngine.UI;

public class HomepageSettings : MonoBehaviour {

    public Toggle VibrationToggle;
    public Toggle SoundToggle;

    void Start()
    {
        VibrationToggle.isOn = (PlayerPrefs.GetInt(PrefsKeys.vibration, 1) == 1);
        SoundToggle.isOn = (PlayerPrefs.GetInt(PrefsKeys.sound, 1) == 1);
    }

    public void OnVibrationToggled(bool isOn){
        PlayerPrefs.SetInt(PrefsKeys.vibration, (isOn) ? 1 : 0);
    }

	public void OnSoundToggled(bool isOn)
	{
        PlayerPrefs.SetInt(PrefsKeys.sound, (isOn) ? 1 : 0);
	}

    public void OnTutorialPressed(){
        Settings.instance.PlayTutorial = true;
        Viker_uicontroller.Instance.LoadUi(K_UI.Gameplay);
        Viker_uicontroller.Instance.ClosePopup();
    }

    public void OnClosePressed(){
        Viker_uicontroller.Instance.ClosePopup();
    }

}
