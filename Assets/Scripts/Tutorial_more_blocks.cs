﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial_more_blocks : MonoBehaviour {

    public GameObject Instructions;
    public Transform TapIcon;
	int _columnChosen;
	bool _waitingForTap;
	float _dampener = 0.1f;
	float _maxSpeed = 1f;

    void OnEnable()
    {
        Gameplay.Instance.DropBlock(2);
        StartCoroutine("PlaySequence");
        _columnChosen = Gameplay.Instance.ColumnForTutorial;
    }

    IEnumerator PlaySequence(){
        yield return new WaitForSeconds(2f);
        UpdateDampener(0f);
        Instructions.SetActive(true);
        PositionTapToColumnChosen();
        TapIcon.gameObject.SetActive(true);
        _waitingForTap = true;
        Gameplay.Instance.EnableColumnChange(false);
        while (_waitingForTap)
        {
            yield return new WaitForSeconds(0.2f);
        }
        UpdateDampener(1f);
        Instructions.SetActive(false);
        TapIcon.gameObject.SetActive(false);
    }


	void PositionTapToColumnChosen()
	{
		Vector3 tapPointInWorld = new Vector3(_columnChosen + 0.5f, 3.5f, 0f);
		TapIcon.position = Camera.main.WorldToScreenPoint(tapPointInWorld);
		TapIcon.gameObject.SetActive(true);
	}

	void UpdateDampener(float newNumber)
	{
		_dampener = newNumber;
		Gameplay.Instance.FallSpeed = _maxSpeed * _dampener;
	}

	void Update()
	{
		if (_waitingForTap)
		{
			if (Input.touchCount > 0)
			{
				// check that the dude has tapped the right column.
				Vector3 worldTap = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
				if (Utilities.roundDownPos(worldTap).x != _columnChosen) return;
				Gameplay.Instance.MoveCurrentBlockToColumn(_columnChosen);
				_waitingForTap = false;
			}
		}
	}
}
