﻿using UnityEngine;
using System.Collections;

public class Tutorial_first_block : MonoBehaviour
{
    public GameObject Instructions;
    [SerializeField] float secsToHideInstructions;
    [SerializeField] float secsToNextStep = 2f;
    [SerializeField] float maxsSpeed = 1f;
    float dampener;
    bool listenForSwipe;
    bool hasSwiped;

    void OnEnable()
    {
        updateDampener(0f);
        Gameplay.Instance.DropBlock(2);
        iTween.ValueTo(gameObject, iTween.Hash("from", dampener, "to", 1f, "time", 1f, "onupdate", "updateDampener", "easetype", iTween.EaseType.easeInQuint));
        StartCoroutine("playSequence");		
    }

    IEnumerator playSequence()
    {
        Gameplay.Instance.EnableSwipeDown(false);
        Gameplay.Instance.EnableColumnChange(false);
        yield return new WaitForSeconds(2f);
        updateDampener(0f);
		Instructions.SetActive(true);
		yield return new WaitForSeconds(2f);
        updateDampener(1f);
        Instructions.SetActive(false);
        yield return new WaitForSeconds(2f);
		Gameplay.Instance.PlayNextOnboardingStep();
    }

    void updateDampener(float newNumber)
    {
        dampener = newNumber;
        Gameplay.Instance.FallSpeed = maxsSpeed * dampener;
	}

}
