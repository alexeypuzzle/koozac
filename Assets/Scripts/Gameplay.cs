﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections.Generic;

public class Gameplay : MonoBehaviour
{
    public static Gameplay Instance;
    public float FallSpeed;
    public GameObject BlockPrefab;
    public GameObject GameOverScreen;
    public GameObject TouchHighlight;
    public TMP_Text TargetText;
    public TMP_Text ScoreText;
    public Transform SpawnPoint;
    public Transform BlockContainer;
    public int CurrentTarget { get { return _currentTarget; } }
    public float YfastSwipeThreshold;
    public GameObject DebugInfo;
    public Text SpeedText;
    public Text IntervalText;
    public Sprite[] NumberSprites;
    public bool IsPaused { get { return _isPaused; } }
    public int Score { get { return _score; } }
    public GameObject Sparkle;
    public List<GameObject> OnBoardingSteps;
    public bool PlayOnboarding;
    public float SpeedDampener;
    public int ColumnForTutorial;
    public GameObject PointsTextGO;
    public TMP_Text PointsText;
    public bool IsFirstGo
    {
        get { return _isFirstGo; }
    }
    int _currentOnboardingStep;
    int _score;
    bool _gameover;
    int _currentTarget;
    int _currentBlockValue;
    int _numberOfCorrectAnswersInARow;
    bool _isPaused;
    Block _currentBlock;
    bool _isFirstGo;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        if (Settings.instance.HasRemovedAds)
        {
            Viker_adsController.Instance.HideBanner();
        }
        else
        {
            Viker_adsController.Instance.ShowBanner();
        }
        FallSpeed = Settings.instance.startingFallSpeed;
        InitializeGame();
    }

    void InitializeGame(){
        
        if (!Settings.instance.PlayTutorial)
        {
            _isFirstGo = true;
            _score = 0;
            ScoreText.text = _score.ToString();
			SetNewBlockValue();
			SpawnBlock();
			SetNewTarget();
			_updateDebugInfo();
		}
		else
		{
			_currentOnboardingStep = 0;
			PlayNextOnboardingStep();
		}
    }

    public void PlayNextOnboardingStep()
    {
        if(_currentOnboardingStep < OnBoardingSteps.Count){
			if (_currentOnboardingStep == 0)
			{
				OnBoardingSteps[_currentOnboardingStep].SetActive(true);
			}
			else
			{
				OnBoardingSteps[_currentOnboardingStep - 1].SetActive(false);
				OnBoardingSteps[_currentOnboardingStep].SetActive(true);
			}
			_currentOnboardingStep++;    
        } else {
            OnBoardingSteps[_currentOnboardingStep - 1].SetActive(false);
            Settings.instance.PlayTutorial = false;
            InitializeGame();
        }
    }

    void SetNewTarget()
    {
        _currentTarget = Random.Range(Settings.instance.minTarget, Settings.instance.maxTarget + 1);
        TargetText.text = _currentTarget.ToString();
    }

    void SetNewBlockValue()
    {
        _currentBlockValue = Random.Range(Settings.instance.minBlockNumber, Settings.instance.maxBlockNumber + 1);
    }

    void SpawnBlock()
    {
        var newBlockGO = Instantiate(BlockPrefab, BlockContainer, false);
        newBlockGO.transform.position = SpawnPoint.position;
        var newBlock = newBlockGO.GetComponent<Block>();
        newBlock.image.sprite = NumberSprites[_currentBlockValue - 1];
        _currentBlock = newBlock;
        newBlock.fallSpeed = FallSpeed;
        newBlock.setValueText(_currentBlockValue);
        if (grid.TrySpawn(newBlock.transform.position)) return;
        
        newBlock.isFalling = false;
        GameOver();
    }

    public void GameOver()
    {
        _isPaused = true;
        _gameover = true;
        GameOverScreen.SetActive(true);
    }

    public void OnBlockPlaced()
    {
        ShowHighlight(false);
        if (_gameover) return;

        if (grid.PlaceAndCheckForBlocksToDestroy(_currentBlock))
        {
            PointsTextGO.transform.position = _currentBlock.transform.position;
            _score += 100;
            ScoreText.text = Score.ToString();
            ShowPointsText();
            _numberOfCorrectAnswersInARow++;
            if (_numberOfCorrectAnswersInARow % Settings.instance.speedIncreaseInterval == 0)
            {
                if (FallSpeed < Settings.instance.maxFallSpeed)
                {
                    FallSpeed += 1f;
                    _updateDebugInfo();
                }
            }
        }
        else
        {
            // if block is placed on a full column - game over.
            if (_currentBlock.transform.position.y == grid.height)
            {
                GameOver();
                return;
            }
            
        }
        SetNewBlockValue();
        if (!Settings.instance.PlayTutorial)
        {
            SetNewTarget();
            SpawnBlock();
        } else {
            ColumnForTutorial = (int)_currentBlock.transform.position.x;
            PlayNextOnboardingStep();
        }
    }

    public void ShowHighlight(bool shouldShow, float columnNumber = 0)
    {
        TouchHighlight.SetActive(shouldShow);
        if (shouldShow)
        {
            TouchHighlight.transform.position = new Vector2(columnNumber, TouchHighlight.transform.position.y);
        }
    }

    public void ResetGame()
    {
        grid.clearGrid();
        for (var i = 0; i < BlockContainer.childCount; i++)
        {
            Destroy(BlockContainer.GetChild(i).gameObject);
        }
        _gameover = false;
        _isPaused = false;
        GameOverScreen.SetActive(false);
        SpawnBlock();
    }

    public void OnContinuePlaying()
    {
        //explode the block that killed the game.
        _currentBlock.Explode();
        _isFirstGo = false;
        grid.DestroyRowsFromTop(4);
        _gameover = false;
        _isPaused = false;
        SpawnBlock();
        GameOverScreen.SetActive(false);
    }

    public void OnSettingsPressed()
    {
        _isPaused = true;
        Viker_uicontroller.Instance.LoadUi(K_UI.Settings);
    }

    public void OnBackPressed()
    {
        Viker_uicontroller.Instance.GoBack();
    }

    void _updateDebugInfo()
    {
        DebugInfo.SetActive(Settings.instance.isDebugModeOn);
        SpeedText.text = "Speed:" + FallSpeed;
        IntervalText.text = "Interval: " + Settings.instance.speedIncreaseInterval;
    }

    public void OnPausePressed()
    {
        _isPaused = true;
        Viker_uicontroller.Instance.ShowPopup(K_UI.Paused);
    }

    public void OnEndGamePressed()
    {
        Viker_uicontroller.Instance.LoadUi(K_UI.MainMenu);
        Destroy(transform.parent.gameObject);
    }

    public void ResumeGame()
    {
        _isPaused = false;
        _updateDebugInfo();
    }

    public void OnDebugPressed()
    {
        _isPaused = true;
        Viker_uicontroller.Instance.ShowPopup(K_UI.Debug);
    }

    public void OnSwiped()
    {
        _currentBlock.slamDown();
    }

    void ShowPointsText(){
        PointsText.text = "+100";
        PointsTextGO.SetActive(true);
    }

#region Tutorial functions
    public void SetTarget(int newTarget)
    {
        _currentTarget = newTarget;
        TargetText.text = _currentTarget.ToString();
    }

    public void DropBlock(int newBlockValue)
    {
        _currentBlockValue = newBlockValue;
        var newBlockGO = Instantiate(BlockPrefab, BlockContainer, false);
        newBlockGO.transform.position = SpawnPoint.position;
        var newBlock = newBlockGO.GetComponent<Block>();
        newBlock.image.sprite = NumberSprites[_currentBlockValue - 1];
        _currentBlock = newBlock;
        newBlock.fallSpeed = FallSpeed;
        newBlock.setValueText(_currentBlockValue);
    }

    public void EnableColumnChange(bool isOn){
        _currentBlock.EnableColumnChange(isOn);
    }

    public void EnableSwipeDown(bool isEnabled)
	{
        _currentBlock.SetSwipeDown(isEnabled);
	}

    public void MoveCurrentBlockToColumn(int column){
        _currentBlock.MoveToColumn(column);
    }

    public void PulseTarget(){
        TargetText.GetComponent<Pulse>().Play();
        print("pusling the target");
    }
#endregion
}
