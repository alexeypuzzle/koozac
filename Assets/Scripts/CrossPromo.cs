﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossPromo : MonoBehaviour {
	string link;
	public void OnClosePressed()
	{
		Viker_uicontroller.Instance.ClosePopup();
	}

	public void OnPlayPressed()
	{
		
		#if UNITY_ANDROID
		link = K_URLS.Amazeballs_android;
		
		#elif UNITY_IOS
		link = K_URLS.Amazeballs_ios;
		#endif
		Application.OpenURL(link);
	}
}
