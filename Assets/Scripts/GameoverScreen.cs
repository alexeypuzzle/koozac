﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class GameoverScreen : MonoBehaviour {

    public Text ScoreText;
    public Text HighScoreText;
	public GameObject ContinuePlayingButtons;
	public GameObject PlayAgainButtons;
	public TMP_Text CountdownText;
	[SerializeField] int _countdownStartingTime;
	
    void OnEnable()
    {
	    ContinuePlayingButtons.SetActive(Gameplay.Instance.IsFirstGo);
	    PlayAgainButtons.SetActive(!Gameplay.Instance.IsFirstGo);
        ScoreText.text = Gameplay.Instance.Score.ToString();
        HighScoreText.text = PlayerPrefs.GetInt(PrefsKeys.highScore, 0).ToString();
	    if (Gameplay.Instance.IsFirstGo)
	    {
		    StartCoroutine("Countdown");    
	    }
    }

	IEnumerator Countdown()
	{
		for (int i = _countdownStartingTime; i >= 0 ; i--)
		{
			CountdownText.text = i.ToString();
			yield return new WaitForSeconds(1f);
		}
		if (Settings.instance.HasRemovedAds)
		{
			Viker_adsController.Instance.ShowInterstitial();
		}
		OnGoHome();
	}

    public void OnPlayAgainPressed(){
        Gameplay.Instance.ResetGame();
    }

    public void OnWatchAdToContinue()
    {
	    print("watch ad to continue");
	    if (Settings.instance.HasRemovedAds)
	    {
		    print("removed ads came back positive");
		    Gameplay.Instance.OnContinuePlaying();
	    }
	    else
	    {
		    print("show ad");
		    Viker_adsController.OnAdWatched += OnAdWatched;
		    Viker_adsController.Instance.ShowRewardedAd();
	    }
    }

	void OnAdWatched()
	{
		if (Gameplay.Instance.IsFirstGo)
		{
			Gameplay.Instance.OnContinuePlaying();
		}
		else
		{
			Gameplay.Instance.ResetGame();
		}
	}

	public void OnReplay()
	{
        Gameplay.Instance.ResetGame();
	}

	public void OnGoHome()
	{
        Gameplay.Instance.OnEndGamePressed();
	}
	public void OnShareScore()
	{
        // TODO take a screenshot and post to FB.
		Gameplay.Instance.OnEndGamePressed();
	}
}
